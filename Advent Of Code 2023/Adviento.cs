﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata.Ecma335;
using System.Xml.Schema;

namespace Advent_Of_Code_2023
{
    internal class Adviento
    {
        static void Main()
        {
            string day, part;
            bool example;

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("-----Advent Of Code 2021-------------");
            Console.Write("\n Introduce el día: ");
            day = Console.ReadLine();

            Console.Write("\n Introduce la parte: ");
            part = Console.ReadLine();
            Console.Clear();

            Console.Write("\n ¿Hacer la prueba? (Y/Any)");

            example = new char[] { 'Y', 'y'}.Contains(Console.ReadKey().KeyChar);
            Console.Clear();
            Console.Write($"Día {day} - Parte {part} ---------------->\n\n");

            Day(day, part, example);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\n\n Pulsa cualquier tecla para salir...");
            Console.ReadKey(true);
        }
        static  void Day(string day, string part, bool example)
        {
            string[] input;

            if(example)
            {
                if(part == "1") input = File.ReadAllLines(@$"..\..\..\inputs\day{day}_prueba.txt");
                else input = File.ReadAllLines(@$"..\..\..\inputs\day{day}_prueba2.txt");
            }
            else input = File.ReadAllLines(@$"..\..\..\inputs\day{day}.txt");

            switch ($"{day}_{part}")
            {
                case "1_1":
                    Day1_1(input);
                    break;
                case "1_2":
                    Day1_2(input);
                    break;
                case "2_1":
                    Day2_1(input);
                    break;
                case "2_2":
                    Day2_2(input);
                    break;
                case "3_1":
                    Day3_1(input);
                    break;
                case "3_2":
                    Day3_2(input);
                    break;
                case "4_1":
                    Day4_1(input);
                    break;
                case "4_2":
                    Day4_2(input);
                    break;
                    /*
                case "5_1":
                    Day5_1(input);
                    break;
                case "5_2":
                    Day5_2(input);
                    break;
                case "6_1":
                    Day6_1(input);
                    break;
                case "6_2":
                    Day6_2(input);
                    break;
                case "7_1":
                    Day7_1(input);
                    break;
                case "7_2":
                    Day7_2(input);
                    break;
                case "8_1":
                    Day8_1(input);
                    break;
                case "8_2":
                    Day8_2(input);
                    break;
                case "9_1":
                    Day9_1(input);
                    break;
                case "9_2":
                    Day9_2(input);
                    break;
                case "10_1":
                    Day10_1(input);
                    break;
                case "10_2":
                    Day10_2(input);
                    break;
                case "11_1":
                    Day11_1(input);
                    break;
                case "11_2":
                    Day11_2(input);
                    break;
                case "12_1":
                    Day12_1(input);
                    break;
                case "12_2":
                    Day12_2(input);
                    break;
                case "13_1":
                    Day13_1(input);
                    break;
                case "13_2":
                    Day13_2(input);
                    break;
                case "14_1":
                    Day14_1(input);
                    break;
                case "14_2":
                    Day14_2(input);
                    break;
                case "15_1":
                    Day15_1(input);
                    break;
                case "15_2":
                    Day15_2(input);
                    break;*/
            }
        }

        static void Day1_1(string[] input)
        {
            int total = 0;
            foreach (string line in input)
            {
                int first = 10, last = 0;
                foreach (char letterCh in line)
                {
                    string letter = char.ToString(letterCh);
                    if (int.TryParse(letter, out int num))
                    {
                        if (first == 10) first = int.Parse(letter);
                        last = int.Parse(letter);
                    }
                }
                Console.WriteLine($"{first} y {last}");
                total += first * 10 + last;
            }
            Console.WriteLine("Total = " + total);
        }
        static void Day1_2(string[] input)
        {
            int total = 0;
            foreach (string line in input)
            {
                int index = 0;
                int first = 10, last = 0, numString = 10;
                foreach (char letter in line)
                {
                    if (char.IsNumber(letter))
                    {
                        if (first == 10) first = int.Parse(letter.ToString());
                        last = int.Parse(letter.ToString());
                    }
                    if (new char[] { 'o', 't', 'f', 's', 'e', 'n' }.Contains(letter))
                    {
                        if (index + 2 < line.Length)
                        {
                            switch (line.Substring(index, 3))
                            {
                                case "one":
                                    numString = 1;
                                    break;
                                case "two":
                                    numString = 2;
                                    break;
                                case "six":
                                    numString = 6;
                                    break;
                            }
                        }
                        if (index + 3 < line.Length)
                        {
                            switch (line.Substring(index, 4))
                            {
                                case "four":
                                    numString = 4;
                                    break;
                                case "five":
                                    numString = 5;
                                    break;
                                case "nine":
                                    numString = 9;
                                    break;
                            }
                        }
                        if (index + 4 < line.Length)
                        {
                            switch (line.Substring(index, 5))
                            {
                                case "three":
                                    numString = 3;
                                    break;
                                case "seven":
                                    numString = 7;
                                    break;
                                case "eight":
                                    numString = 8;
                                    break;
                            }
                        }
                    }
                    else numString = 10;

                    if(numString != 10)
                    {
                        if (first == 10) first = numString;
                        last = numString;
                    }

                    index++;
                }
                Console.WriteLine($"{first} y {last}");
                total += first * 10 + last;
            }
            Console.WriteLine("Total = " + total);
        }

        static void Day2_1(string[] input)
        {
            int maxRed = 12;
            int maxGreen = 13;
            int maxBlue = 14;
            int total = 0;

            foreach (string line in input)
            {
                bool result = true;
                foreach (string round in line.Split(':')[1].Split(';'))
                {
                    foreach (var pack in round.Split(','))
                    {
                        switch (pack.Substring(1).Split(' ')[1])
                        {
                            case "red":
                                if (int.Parse(pack.Substring(1).Split(' ')[0]) > maxRed) result = false;
                                break;
                            case "green":
                                if (int.Parse(pack.Substring(1).Split(' ')[0]) > maxGreen) result = false;
                                break;
                            case "blue":
                                if (int.Parse(pack.Substring(1).Split(' ')[0]) > maxBlue) result = false;
                                break;
                            default:
                                Console.WriteLine("No encontrado");
                                break;
                        }
                    }
                }
                Console.WriteLine($"{line.Substring(0, 6)} {result}");
                if (result) total += int.Parse(line.Split(':')[0].Split(' ')[1]);
            }
            Console.WriteLine("Total = " + total);
        }
        static void Day2_2(string[] input)
        {
            int total = 0;

            foreach (string line in input)
            {
                int maxRed = 0;
                int maxGreen = 0;
                int maxBlue = 0;

                foreach (string round in line.Split(':')[1].Split(';'))
                {
                    foreach (var pack in round.Split(','))
                    {
                        int value = int.Parse(pack.Substring(1).Split(' ')[0]);
                        switch (pack.Substring(1).Split(' ')[1])
                        {
                            case "red":
                                if (value > maxRed) maxRed = value;
                                break;
                            case "green":
                                if (value > maxGreen) maxGreen = value;
                                break;
                            case "blue":
                                if (value > maxBlue) maxBlue = value;
                                break;
                        }
                    }
                }
                total += maxRed * maxBlue * maxGreen;
            }
            Console.WriteLine("Total = " + total);
        }

        static void Day3_1(string[] input)
        {
            int total = 0;
            for (int a = 0; a < input.Length; a++)
            {
                bool valid = false;
                string numSt = "";
                for (int b = 0; b < input[a].Length; b++)
                {
                    if (int.TryParse(input[a][b].ToString(), out int ignore))
                    {
                        numSt += input[a][b].ToString();
                        if(!valid)
                        {
                            //Compruebo Izquierda
                            if (b - 1 >= 0) if (!CheckInt(input[a][b - 1]) && input[a][b - 1] != '.') valid = true;
                            //Compruebo Derecha
                            if (b + 1 < input[a].Length) if (!CheckInt(input[a][b + 1]) && input[a][b + 1] != '.') valid = true;

                            //Compruebo Arriba
                            if (a - 1 >= 0) if (!CheckInt(input[a - 1][b]) && input[a - 1][b] != '.') valid = true;
                            //Compruebo Abajo
                            if (a + 1 < input[a].Length) if (!CheckInt(input[a + 1][b]) && input[a + 1][b] != '.') valid = true;

                            //Compruebo Arriba-Izquierda
                            if (a - 1 >= 0 && b - 1 >= 0) if (!CheckInt(input[a - 1][b - 1]) && input[a - 1][b - 1] != '.') valid = true;
                            //Compruebo Arriba-Derecha
                            if (a - 1 >= 0 && b + 1 < input[a].Length) if (!CheckInt(input[a - 1][b + 1]) && input[a - 1][b + 1] != '.') valid = true;
                            //Compruebo Abajo-Izquierda
                            if (a + 1 < input[a].Length && b - 1 >= 0) if (!CheckInt(input[a + 1][b - 1]) && input[a + 1][b - 1] != '.') valid = true;
                            //Compruebo Abajo-Derecha
                            if (a + 1 < input[a].Length && b + 1 < input[a].Length) if (!CheckInt(input[a + 1][b + 1]) && input[a + 1][b + 1] != '.') valid = true;
                        }
                    }
                    if(!int.TryParse(input[a][b].ToString(), out int ignore2) || b == input[a].Length - 1)
                    {
                        if (numSt != "")
                        {
                            if (valid)
                            {
                                total += int.Parse(numSt);
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write(numSt);
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write(numSt);
                            }
                        }
                        Console.ForegroundColor = ConsoleColor.White;
                        if(b != input[a].Length - 1 || (b == input[a].Length - 1 && input[a][b] == '.')) Console.Write(input[a][b]);
                        valid = false;
                        numSt = "";
                    }
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nTotal = " + total);
        }
        static bool CheckInt(char value) { return int.TryParse(value.ToString(), out int ignore); }
        static void Day3_2(string[] input)
        {
            int total = 0;
            var map = new Dictionary<string, int>();            
            for (int a = 0; a < input.Length; a++)
            {
                bool valid = false;
                string numSt = "";
                string ast = "";
                for (int b = 0; b < input[a].Length; b++)
                {
                    if (int.TryParse(input[a][b].ToString(), out int ignore))
                    {
                        numSt += input[a][b].ToString();
                        if (!valid)
                        {
                            //Compruebo Izquierda
                            if (b - 1 >= 0) if (input[a][b - 1] == '*')
                                {
                                    valid = true;
                                    ast = a + "," + (b - 1);
                                }
                            //Compruebo Derecha
                            if (b + 1 < input[a].Length) if (input[a][b + 1] == '*')
                                {
                                    valid = true;
                                    ast = a + "," + (b + 1);
                                }

                            //Compruebo Arriba
                            if (a - 1 >= 0) if (input[a - 1][b] == '*')
                                {
                                    valid = true;
                                    ast = (a - 1) + "," + b;
                                }
                            //Compruebo Abajo
                            if (a + 1 < input[a].Length) if (input[a + 1][b] == '*')
                                {
                                    valid = true;
                                    ast = (a + 1) + "," + b;
                                }

                            //Compruebo Arriba-Izquierda
                            if (a - 1 >= 0 && b - 1 >= 0) if (input[a - 1][b - 1] == '*')
                                {
                                    valid = true;
                                    ast = (a - 1) + "," + (b - 1);
                                }
                            //Compruebo Arriba-Derecha
                            if (a - 1 >= 0 && b + 1 < input[a].Length) if (input[a - 1][b + 1] == '*')
                                {
                                    valid = true;
                                    ast = (a - 1) + "," + (b + 1);
                                }
                            //Compruebo Abajo-Izquierda
                            if (a + 1 < input[a].Length && b - 1 >= 0) if (input[a + 1][b - 1] == '*')
                                {
                                    valid = true;
                                    ast = (a + 1) + "," + (b - 1);
                                }
                            //Compruebo Abajo-Derecha
                            if (a + 1 < input[a].Length && b + 1 < input[a].Length) if (input[a + 1][b + 1] == '*')
                                {
                                    valid = true;
                                    ast = (a + 1) + "," + (b + 1);
                                }
                        }
                    }
                    if (!int.TryParse(input[a][b].ToString(), out int ignore2) || b == input[a].Length - 1)
                    {
                        if (numSt != "")
                        {
                            if (valid)
                            {
                                Console.WriteLine($"Buscando número con asterisco: {ast}    - {map.TryGetValue(ast, out int ignore3)}");
                                if(map.TryGetValue(ast, out int num))
                                {
                                    total += num * int.Parse(numSt);
                                }
                                else
                                {
                                    map.Add(ast, int.Parse(numSt));
                                }
                            }
                        }
                        valid = false;
                        numSt = "";
                    }
                }
            }
            Console.WriteLine("\nTotal = " + total);
        }
        static void Day4_1(string[] input)
        {
            int total = 0;

            foreach (string line in input)
            {
                List<int> winners = new List<int>();
                int hits = 0;
                bool winnersEnd = false;
                foreach (string numberSt in line.Split(':')[1].Split(' '))
                {
                    if(!winnersEnd)
                    {
                        if(numberSt == "|" || int.TryParse(numberSt, out int num))
                        {
                            if (numberSt != "|") winners.Add(int.Parse(numberSt));
                            else winnersEnd = true;
                        }
                    }
                    else
                    {
                        if (int.TryParse(numberSt, out int num) && winners.Contains(int.Parse(numberSt))) hits++;
                    }
                }
                switch(hits)
                {
                    case 0:
                        break;
                    default:
                        total += Convert.ToInt32(Math.Pow(2,hits - 1));
                        break;
                }
                Console.WriteLine($"{line.Split(':')[0]}: {hits} hits");
            }
            Console.WriteLine($"Total = {total}");
        }
        static void Day4_2(string[] input)
        {
            int index = 0;
            int[] quantity = new int[input.Length];
            for (int i = 0; i < quantity.Length; i++)
            {
                quantity[i] = 1;
            }

            foreach (string line in input)
            {
                List<int> winners = new List<int>();
                int hits = 0;
                bool winnersEnd = false;
                foreach (string numberSt in line.Split(':')[1].Split(' '))
                {
                    if (!winnersEnd)
                    {
                        if (numberSt == "|" || int.TryParse(numberSt, out int num))
                        {
                            if (numberSt != "|") winners.Add(int.Parse(numberSt));
                            else winnersEnd = true;
                        }
                    }
                    else
                    {
                        if (int.TryParse(numberSt, out int num) && winners.Contains(int.Parse(numberSt))) hits++;
                    }
                }
                for (int i = index + 1; i <= index + hits && i < quantity.Length;i++)
                {
                    quantity[i] += quantity[index];
                }
                index++;
            }
            Console.WriteLine("Total = " + quantity.Sum());
        }
    }
}

namespace AdventOfCode2022
{

}
